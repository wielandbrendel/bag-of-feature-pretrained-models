# Pretrained weights for bag-of-feature models

This repository holds the pretrained weights for the models published @ https://github.com/wielandbrendel/bag-of-local-features-models